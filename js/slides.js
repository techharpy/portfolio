$(function(){



	$('.slider-banner').slick({
		accessibility: false,				
		arrows: false,
		autoplay: true,
		autoplaySpeed: 2000,		
		speed: 2000,
		slidesToShow: 3,
		slidesToScroll: 3,
		
		responsive: [
		{
			breakpoint: 720,
			settings: {
				slidesToShow: 2,
				slidesToScroll: 2,
			}
		},
		{
			breakpoint: 500,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1,
			}
		}

		]
	});
	



});