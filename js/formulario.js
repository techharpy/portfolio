$(function(){
    $('body').on('submit','form',function(){
        var form = $(this);
        $.ajax({
            beforeSend:function(){
                $('#enviarform').prop("disabled",true);
                $('.overlay-loading').fadeIn();
            },
            url:include_path+'ajax/formulario.php',
            method:'post',
            dataType: 'json',
            data:form.serialize()
        }).done(function(data){
            if(data.sucesso){
                //deu certo... vamos estilizar
                $('#enviarform').prop("disabled",false);
                alert("Entrarei em contato o mais rápido possível!") 
                $('.overlay-loading').fadeOut();             
                $('.sucesso').fadeIn();
                setTimeout(function(){
                    $('.sucesso').fadeOut();
                },3000)
            }else{
                $('.erro').fadeIn();
                setTimeout(function(){
                    $('.erro').fadeOut();
                },3000)
                $('.overlay-loading').fadeOut();
                console.log("Erro ao enviar e-mail!")
                alert("Ocorreu algum erro")
            }
        });
        return false;
    })
})