<?php include('config.php'); ?>

<?php
	$url = isset($_GET['url']) ? $_GET['url'] : 'index';
	switch ($url) {
		case 'ofertas':
			echo '<target target="ofertas" />';
			break;

		case 'habilidades':
			echo '<target target="habilidades" />';
			break;


	}
?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="utf-8">
	<title>Desenvolvedor Web | Rio de Janeiro</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<meta name="keywords" content="desenvolvedor web rio de janeiro ,desenvolvedor frontend rj, design web rio de janeiro, profissional web rio de janeiro, desenvolvedor web no rio de janeiro, criador de sites rio de janeiro" />
	<meta name="description" content="Desenvolvedor Web Rio de Janeiro, crie seu site para sua empresa com um profissional que entende do negócio, traga mais clientes para seu negócio, amplie seus horizontes!">
	<meta name="author" content="Renan Lima">
	<link rel="stylesheet" href="css/slick.css">
	<link rel="stylesheet" href="css/style.css">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Lora|Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">	
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">
	<link rel="icon" href="img/icone.png" />
	<link rel="canonical" href="https://renanlimaweb.com.br/"/>
	<!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-120309926-1"></script>

</head>
<body>

<base base="<?php echo INCLUDE_PATH; ?>" />

<a id="btn_subir" class="botao-subir" href="#header"><i class="fa fa-arrow-up" aria-hidden="true"></i></a>

<header id="header">
	<a href="https://renanlimaweb.com.br">
		<div class="logo">
			<h2><span style="color:#ffc35b">Renan</span> Lima</h2>
			<p>Desenvolvedor <span style="color:#ffc35b">web</span></p>
		</div><!--logo-->
	</a>	
		<nav class="desktop">
			<ul>
				<li><a href="<?php echo INCLUDE_PATH; ?>">Início</a></li>
				<li><a ref_sys="portfolio">Portfólio</a></li>
				<li><a ref_sys="valeAP">Vantagens</a></li>
				<li><a style="color:#ffc35b" ref_sys="contato">Contato</a></li>
				<li><a href="#">Cursos</a></li>
			</ul>
		</nav><!--desktop-->

		<nav id="mobile" class="mobile">
			<h3><i class="fa fa-bars" aria-hidden="true"></i></h3>
			<ul>
				<li><a href="<?php echo INCLUDE_PATH; ?>">Início</a></li>
				<li><a ref_sys="portfolio">Portfólio</a></li>
				<li><a ref_sys="valeAP">Vale a Pena?</a></li>
				<li><a style="color:#ffc35b" ref_sys="contato">Contato</a></li>
				<li><a href="#">Cursos</a></li>
			</ul>
		</nav><!--mobile-->
		<div class="clear"></div>			
</header>

<section class="banner">
	<div class="mascara-banner"></div>
	<div class="container">	

		<div class="chamada1-banner">			
			<div class="texto-banner">
				<h1>Desenvolvedor Web | Rio de Janeiro</h1>				
				<h2 id="h2-banner-responsivo">Produção de sites e sistemas webs totalmente funcionais e <span style="color: white;">responsivos.</span><i class="fas fa-plus"></i></h2>
				<p id="p-banner-responsivo">Tenha um site que se adapta em <span style="color: white;">qualquer dispositivo</span> (smartphone, tablets, desktops).</p>
				<h2 id="h2-banner-seo">Seu site otimizado para se <span style="color: white;">destacar</span> no Google.<i class="fas fa-plus"></i></h2>				
				<p id="p-banner-seo">Você na frente da concorrência! Seu site ficará nas <span style="color: white;">primeiras colocações</span> do Google e atingirá a maioria do público.</p>			
				
				<span class="btn1" ref_sys="portfolio">Ver portfólio</span>
					
			</div><!--texto-banner-->							
		</div><!--chamada1-banner-->		

	</div><!--container-->


	<div class="slider-banner">
		<div class="slider-banner-img" style="background-image:url('img/node.png');"></div><!--slider-banner-img-->
		<div class="slider-banner-img" style="background-image:url('img/php.png');"></div><!--slider-banner-img-->
		<div class="slider-banner-img" style="background-image:url('img/git.png');"></div><!--slider-banner-img-->
		<div class="slider-banner-img" style="background-image:url('img/html.png');"></div><!--slider-banner-img-->
		<div class="slider-banner-img" style="background-image:url('img/css.png');"></div><!--slider-banner-img-->
		<div class="slider-banner-img" style="background-image:url('img/js.png');"></div><!--slider-banner-img-->
		<div class="slider-banner-img" style="background-image:url('img/bootstrap.png');"></div><!--slider-banner-img-->
		<div class="slider-banner-img" style="background-image:url('img/materalize.png');"></div><!--slider-banner-img-->
		<div class="slider-banner-img" style="background-image:url('img/ux-ui.png');"></div><!--slider-banner-img-->
		<div class="slider-banner-img" style="background-image:url('img/sass.png');"></div><!--slider-banner-img-->
		<div class="slider-banner-img" style="background-image:url('img/less.png');"></div><!--slider-banner-img-->
		<div class="slider-banner-img" style="background-image:url('img/jquery.png');"></div><!--slider-banner-img-->
		
	</div><!--slider-banner-->
</section><!--banner-->

<section id="portfolio" class="chamada-portfolio">
	<div class="container">
		<div class="texto-portfolio">
			<h2>Muito bem-vindo(a) ao meu portfólio!</h2>
			<p>Visite o site de alguns clientes.</p>	
			<p class="valeAPena"><b style="color: #000">Vale a pena ter um site?</b> <span ref_sys="valeAP">Dê uma olhada nisso!</span></p>		
		</div>
	</div>
</section>

<section class="portfolio">
	<div class="container">
		<div class="portfolio-single">
			<img src="img/fatimaleite.png">
			<a target="_blank" href="https://fatimaleitefotografia.com">Visite!</a>
		</div>		
	</div>	
</section>

<section id="valeAP" class="sobre">	
	<div class="container">
		<div class="marketing-wrapper">			
			<div class="marketing-single">
				<p class="citacao">"A TECNOLOGIA MOVE O MUNDO" - Steve Jobs</p>
				<p><b>Veja alguns dados interessantes!</b></p>				
				<img modal="" src="img/grafico2.jpg">
			</div>	
			<div class="marketing-single">
				<p class="citacao">"EM ALGUNS ANOS VÃO EXISTIR DOIS TIPOS DE EMPRESAS: AS QUE FAZEM NEGÓCIOS PELA INTERNET E AS QUE ESTÃO FORA DOS NEGÓCIOS" - Bill Gates</p>
				<p><b>Brasileiros ocupam a 3ª COLOCAÇÃO entre os usuários mais intensivos da internet!</b></p>
				<img modal="" src="img/grafico1.jpg">
			</div>	
			<div class="entre-contato">
				<a ref_sys="contato">Entre em contato comigo =)</a>
			</div>
		</div>		
	</div>	
</section><!--sobre-->

<section id="contato" class="contato">
	<h2>Alguma ideia para seu negócio? Entre em contato comigo =)</h2>
	<div class="container">
		<form method="post" autocomplete="off" action="">
			<input type="text" name="nome" placeholder="Qual seu nome?" required>
			<input type="email" name="email" placeholder="Qual seu e-mail?" required>
			<textarea required placeholder="Digite sua mensagem" name="mensagem"></textarea>		
			<input id="enviarform" type="submit" name="acao" value="Enviar">
		</form>
	</div><!--container-->
</section><!--contato-->

<div class="overlay-loading">
    <img src="<?php echo INCLUDE_PATH; ?>img/ajax-loader.gif" alt="">
</div>
<div class="sucesso">Formulário enviado com sucesso</div>
<div class="erro">Formulário não enviado</div>


<footer>
	<div class="container">
		<div class="footer-descricao">
			<h3>Todos os direito reservados.</h3>
			<p>Renan Lima - 2018.</p>
			<a alt="Meu Facebook" rel="noopener" target="_blank" href="https://www.facebook.com/renan.lima.7393"><i class="fab fa-facebook-f"></i></a>
			<a alt="Meu Instagram" rel="noopener" target="_blank" href="https://www.instagram.com/renanlimabl/"><i class="fab fa-instagram"></i></a>
		</div>
	</div><!--container-->
</footer>

	<script src="js/jquery.js"></script>
	<script src="js/constants.js"></script>
	<script src="js/functions.js"></script>
	<script src="js/formulario.js"></script>
	<script src="js/slick.js"></script>
	<script src="js/slides.js"></script>
	
	
	 <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
    
      gtag('config', 'UA-120309926-1');
    </script>
</body>
</html>